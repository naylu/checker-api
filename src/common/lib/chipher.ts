import * as bcrypt from 'bcryptjs';

export class Chipher {
  static hash(password: string, salt = 10): Promise<string> {
    return bcrypt.hash(password, salt);
  }

  static compare(blank: string, hashed: string): Promise<boolean> {
    return bcrypt.compare(blank, hashed);
  }
}
