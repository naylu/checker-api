import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from 'src/modules/user';
import { Chipher } from '../lib';

@Injectable()
export class AuthService {
  static secret = '3573 N0 35 UN S3CR37';

  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  async validateUserLocal(username: string, password: string) {
    const user = await this.userService.findUserByUsername(username);

    if (!user) {
      throw new UnauthorizedException();
    }

    const passwordMatch = await Chipher.compare(password, user.password);

    if (!passwordMatch) {
      throw new UnauthorizedException();
    }

    delete user.password;

    return user;
  }

  async generateToken(user: Express.User) {
    return this.jwtService.sign({ sub: user.id });
  }

  async validateUserJWT(sub: number) {
    const user = await this.userService.findUserById(sub);

    if (!user) {
      throw new UnauthorizedException();
    }

    return user;
  }
}
