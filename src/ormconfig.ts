import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions'
import { Checker } from './modules/checker/checker.entity'
import { Employee } from './modules/employee/employee.entity'
import { RoleSchedule } from './modules/role-schedule/schedule-role.entity'
import { User } from './modules/user'

export interface DatabaseOptions {
    host: string
    port: number
    username: string
    password: string
    database: string
}

export const buildConfig = (
    options: DatabaseOptions,
): MysqlConnectionOptions => ({
    type: 'mysql',
    host: options.host,
    port: options.port,
    username: options.username,
    password: options.password,
    database: options.database,
    entities: [User, RoleSchedule, Employee, Checker],
    subscribers: [],
})
