import { MigrationInterface, QueryRunner } from 'typeorm';

export class RoleSchedule1615783175930 implements MigrationInterface {
  name = 'RoleSchedule1615783175930';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      "CREATE TABLE `role_schedule` (`id` int NOT NULL AUTO_INCREMENT, `role` enum ('manager', 'boss', 'employee', 'employeeHalf') NOT NULL DEFAULT 'employee', `startWork` varchar(255) NOT NULL, `endWork` varchar(255) NOT NULL, `startLunch` varchar(255) NULL, `endLunch` varchar(255) NULL, UNIQUE INDEX `IDX_31217a67954dca1edd83f2b36d` (`role`), PRIMARY KEY (`id`)) ENGINE=InnoDB",
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'DROP INDEX `IDX_31217a67954dca1edd83f2b36d` ON `role_schedule`',
    );
    await queryRunner.query('DROP TABLE `role_schedule`');
  }
}
