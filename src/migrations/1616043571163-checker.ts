import {MigrationInterface, QueryRunner} from "typeorm";

export class checker1616043571163 implements MigrationInterface {
    name = 'checker1616043571163'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `checker` (`id` int NOT NULL AUTO_INCREMENT, `date` varchar(255) NOT NULL, `hour` varchar(255) NOT NULL, `employeeId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `checker` ADD CONSTRAINT `FK_bd8e4a974322b722e180aa51486` FOREIGN KEY (`employeeId`) REFERENCES `employee`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `checker` DROP FOREIGN KEY `FK_bd8e4a974322b722e180aa51486`");
        await queryRunner.query("DROP TABLE `checker`");
    }

}
