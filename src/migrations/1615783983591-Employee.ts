import { MigrationInterface, QueryRunner } from 'typeorm';

export class Employee1615783983591 implements MigrationInterface {
  name = 'Employee1615783983591';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      "CREATE TABLE `employee` (`id` int NOT NULL AUTO_INCREMENT, `firstName` varchar(255) NOT NULL, `lastName` varchar(255) NOT NULL, `mLastName` varchar(255) NULL, `phone` varchar(10) NOT NULL, `checkCode` varchar(255) NOT NULL, `role` enum ('manager', 'boss', 'employee', 'employeeHalf') NOT NULL DEFAULT 'employee', `startWork` varchar(255) NOT NULL, `endWork` varchar(255) NOT NULL, `startLunch` varchar(255) NULL, `endLunch` varchar(255) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB",
    );

    await queryRunner.query('ALTER TABLE `employee` AUTO_INCREMENT = 6');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('DROP TABLE `employee`');
  }
}
