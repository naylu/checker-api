import { User } from 'src/modules/user'
import { MigrationInterface, QueryRunner } from 'typeorm'

export class CreateUser1616129251935 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        const repo = queryRunner.manager.getRepository(User)
        const admin = repo.create({
            firstName: 'Manuel',
            lastName: 'Leal',
            username: 'admin',
            password: 'admin1234',
        })

        await repo.save(admin)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        const repo = queryRunner.manager.getRepository(User)

        const admin = await repo.findOne({ username: 'admin' })

        await repo.remove(admin)
    }
}
