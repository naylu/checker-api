import { EmployeeRole } from 'src/modules/role-schedule/role.enum'
import { RoleSchedule } from 'src/modules/role-schedule/schedule-role.entity'
import { MigrationInterface, QueryRunner } from 'typeorm'

export class CreateRole1616128715718 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        const repo = queryRunner.manager.getRepository(RoleSchedule)
        const manager = repo.create({
            role: EmployeeRole.manager,
            startWork: '09:00',
            endWork: '19:00',
            startLunch: '13:00',
            endLunch: '15:00',
        })
        const boss = repo.create({
            role: EmployeeRole.boss,
            startWork: '09:00',
            endWork: '18:00',
            startLunch: '13:00',
            endLunch: '14:00',
        })
        const employee = repo.create({
            role: EmployeeRole.employee,
            startWork: '09:00',
            endWork: '18:00',
            startLunch: '13:00',
            endLunch: '14:00',
        })

        await repo.save(manager)
        await repo.save(boss)
        await repo.save(employee)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        const repo = queryRunner.manager.getRepository(RoleSchedule)

        const manager = await repo.findOne({ role: EmployeeRole.manager })
        const boss = await repo.findOne({ role: EmployeeRole.boss })
        const employee = await repo.findOne({ role: EmployeeRole.employee })

        await repo.remove(manager)
        await repo.remove(boss)
        await repo.remove(employee)
    }
}
