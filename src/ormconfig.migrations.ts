import * as dotenv from 'dotenv';
import { buildConfig } from './ormconfig';
import { env } from 'process';

dotenv.config();

export default {
  ...buildConfig({
    host: env.DB_HOST,
    port: Number(env.DB_PORT),
    username: env.DB_USER,
    password: env.DB_PASS,
    database: env.DB_NAME,
  }),
  migrations: ['src/migrations/*{.ts,.js}'],
  cli: {
    migrationsDir: 'src/migrations',
  },
};
