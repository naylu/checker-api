declare namespace Express {
  interface User {
    id: number;
    firstName: string;
    lastName: string;
    username: string;
  }

  interface Request {
    user?: User;
  }
}
