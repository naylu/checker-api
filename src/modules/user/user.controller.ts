import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { Private } from 'src/common/auth/auth.guard';
import { CreateUserInput } from './create-user.input';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @UseGuards(Private)
  @Get(':id')
  async findOne(@Param('id') id: string) {
    return this.userService.findUserById(Number(id));
  }

  @Post()
  async create(@Body() createUserInput: CreateUserInput) {
    return this.userService.create(createUserInput);
  }
}
