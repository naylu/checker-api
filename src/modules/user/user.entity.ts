import { Chipher } from 'src/common/lib';
import { Entity, Column, BeforeInsert, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ unique: true })
  username: string;

  @Column()
  password: string;

  @BeforeInsert()
  async encryptPassword() {
    this.password = await Chipher.hash(this.password);
  }
}
