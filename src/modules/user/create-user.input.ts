import { User } from './user.entity';

export class CreateUserInput implements Partial<User> {
  firstName: string;
  lastName: string;
  username: string;
  password: string;
}
