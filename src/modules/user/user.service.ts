import { Injectable } from '@nestjs/common';
import { InjectEntityManager } from '@nestjs/typeorm';
import { EntityManager } from 'typeorm';
import { User } from './user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectEntityManager()
    private readonly entityManager: EntityManager,
  ) {}

  findUserByUsername(username: string) {
    const repo = this.getRepository();
    return repo.findOne({
      where: {
        username,
      },
    });
  }

  findUserById(id: number) {
    const repo = this.getRepository();
    return repo.findOne({
      select: this.getPublicSelect(),
      where: {
        id,
      },
    });
  }

  async create(data: Partial<User>) {
    const repo = this.getRepository();
    const user = repo.create({
      ...data,
    });

    const newUser = await repo.save(user);

    return {
      id: newUser.id,
    };
  }

  private getPublicSelect(): (keyof User)[] {
    return ['id', 'firstName', 'lastName', 'username'];
  }

  private getRepository(entityManager: EntityManager = this.entityManager) {
    return entityManager.getRepository(User);
  }
}
