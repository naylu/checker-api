import { Injectable } from '@nestjs/common'
import { InjectEntityManager } from '@nestjs/typeorm'
import { EntityManager } from 'typeorm'
import { Employee } from './employee.entity'

@Injectable()
export class EmployeeService {
    constructor(
        @InjectEntityManager()
        private readonly entityManager: EntityManager,
    ) {}

    getEmployee(id: number) {
        return this.getRepository().findOne({ where: { id } })
    }

    list() {
        return this.getRepository().find({
            select: this.getPublicSelect(),
        })
    }

    async create(data: Partial<Employee>) {
        const repo = this.getRepository()
        const user = repo.create({
            ...data,
        })

        const newEmployee = await repo.save(user)

        return {
            id: newEmployee.id,
        }
    }

    private getPublicSelect(): (keyof Employee)[] {
        return ['id', 'firstName', 'lastName', 'mLastName', 'role', 'phone']
    }

    private getRepository(entityManager: EntityManager = this.entityManager) {
        return entityManager.getRepository(Employee)
    }
}
