import { Length } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { EmployeeRole } from '../role-schedule/role.enum';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ nullable: true })
  mLastName?: string;

  @Length(10, 10)
  @Column({ length: 10 })
  phone: string;

  @Column()
  checkCode: string;

  @Column('enum', { enum: EmployeeRole, default: EmployeeRole.employee })
  role: EmployeeRole;

  @Column()
  startWork: string;

  @Column()
  endWork: string;

  @Column({ nullable: true })
  startLunch?: string;

  @Column({ nullable: true })
  endLunch?: string;
}
