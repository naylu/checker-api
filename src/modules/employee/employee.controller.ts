import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common'
import { Private } from 'src/common/auth'
import { CreateEmployeeInput } from './create-employee.input'
import { EmployeeService } from './employee.service'

@Controller('employee')
export class EmployeeController {
    constructor(private employeeService: EmployeeService) {}

    @UseGuards(Private)
    @Get()
    list() {
        return this.employeeService.list()
    }

    @UseGuards(Private)
    @Get(':id')
    async findOne(@Param('id') id: string) {
        return this.employeeService.getEmployee(Number(id))
    }

    @UseGuards(Private)
    @Post()
    create(@Body() createEmployeeInput: CreateEmployeeInput) {
        return this.employeeService.create(createEmployeeInput)
    }
}
