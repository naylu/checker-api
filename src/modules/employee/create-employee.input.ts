import { IsEnum, IsOptional, Length } from 'class-validator'
import { EmployeeRole } from '../role-schedule/role.enum'
import { Employee } from './employee.entity'

export class CreateEmployeeInput implements Partial<Employee> {
    firstName: string

    lastName: string

    @IsOptional()
    mLastName?: string

    @Length(10, 10)
    phone: string

    checkCode: string

    @IsEnum(EmployeeRole)
    role: EmployeeRole

    startWork: string

    endWork: string

    @IsOptional()
    startLunch?: string

    @IsOptional()
    endLunch?: string
}
