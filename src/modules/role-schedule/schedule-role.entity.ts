import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { EmployeeRole } from './role.enum';

@Entity()
export class RoleSchedule {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column('enum', {
    enum: EmployeeRole,
    default: EmployeeRole.employee,
    unique: true,
  })
  role: EmployeeRole;

  @Column()
  startWork: string;

  @Column()
  endWork: string;

  @Column({ nullable: true })
  startLunch?: string;

  @Column({ nullable: true })
  endLunch?: string;
}
