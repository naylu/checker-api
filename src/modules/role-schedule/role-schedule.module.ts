import { Module } from '@nestjs/common';
import { RoleScheduleController } from './role-schedule.controller';
import { RoleScheduleService } from './role-schedule.service';

@Module({
  controllers: [RoleScheduleController],
  providers: [RoleScheduleService],
})
export class RoleScheduleModule {}
