import { Injectable } from '@nestjs/common';
import { InjectEntityManager } from '@nestjs/typeorm';
import { EntityManager } from 'typeorm';
import { RoleSchedule } from './schedule-role.entity';

@Injectable()
export class RoleScheduleService {
  constructor(
    @InjectEntityManager()
    private readonly entityManager: EntityManager,
  ) {}

  list() {
    return this.getRepository().find();
  }

  async upsert(data: Partial<RoleSchedule>) {
    const repo = this.getRepository();
    const user = repo.create({
      ...data,
    });

    const newRoleSchedule = await repo.save(user);

    return {
      id: newRoleSchedule.id,
    };
  }

  private getRepository(entityManager: EntityManager = this.entityManager) {
    return entityManager.getRepository(RoleSchedule);
  }
}
