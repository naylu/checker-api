export enum EmployeeRole {
  manager = 'manager',
  boss = 'boss',
  employee = 'employee',
  employeeHalf = 'employeeHalf',
}
