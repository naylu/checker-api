import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { Private } from 'src/common/auth';
import { CreateRoleScheduleInput } from './create-role-schedule.input';
import { RoleScheduleService } from './role-schedule.service';

@Controller('roleSchedule')
export class RoleScheduleController {
  constructor(private roleScheduleService: RoleScheduleService) {}

  @UseGuards(Private)
  @Get()
  list() {
    return this.roleScheduleService.list();
  }

  @UseGuards(Private)
  @Post()
  create(@Body() createRoleScheduleInput: CreateRoleScheduleInput) {
    return this.roleScheduleService.upsert(createRoleScheduleInput);
  }
}
