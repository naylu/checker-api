import { IsEnum, IsOptional } from 'class-validator';
import { EmployeeRole } from './role.enum';
import { RoleSchedule } from './schedule-role.entity';

export class CreateRoleScheduleInput implements Partial<RoleSchedule> {
  @IsEnum(EmployeeRole)
  role: EmployeeRole;

  startWork: string;

  endWork: string;

  @IsOptional()
  startLunch?: string;

  @IsOptional()
  endLunch?: string;
}
