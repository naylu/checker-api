import { Module } from '@nestjs/common'
import { EmployeeService } from '../employee/employee.service'
import { CheckerController } from './checker.controller'
import { CheckerService } from './checker.service'

@Module({
    controllers: [CheckerController],
    providers: [CheckerService, EmployeeService],
})
export class CheckerModule {}
