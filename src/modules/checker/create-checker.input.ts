import { Checker } from './checker.entity'

export class CreateCheckerInput implements Partial<Checker> {
    date: string

    hour: string

    employeeId: number

    checkCode: string
}
