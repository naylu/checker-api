import {
    Column,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm'
import { Employee } from '../employee/employee.entity'

@Entity()
export class Checker {
    @PrimaryGeneratedColumn('increment')
    id: number

    @JoinColumn()
    @ManyToOne(() => Employee)
    employee: Employee

    @Column()
    date: string

    @Column()
    hour: string
}
