import { Checker } from './checker.entity'

export class ListCheckerInput implements Partial<Checker> {
    date: string

    employeeId: number
}
