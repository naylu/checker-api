import { Injectable } from '@nestjs/common'
import { InjectEntityManager } from '@nestjs/typeorm'
import { EntityManager } from 'typeorm'
import { Employee } from '../employee/employee.entity'
import { Checker } from './checker.entity'

@Injectable()
export class CheckerService {
    constructor(
        @InjectEntityManager()
        private readonly entityManager: EntityManager,
    ) {}

    list() {
        return this.getRepository().find()
    }

    getCheckerDate(date: string, employee: Employee) {
        return this.getRepository().find({ where: { date, employee } })
    }

    async create(data: Partial<Checker>) {
        const repo = this.getRepository()
        const user = repo.create({
            ...data,
        })

        const newChecker = await repo.save(user)

        return {
            id: newChecker.id,
        }
    }

    private getRepository(entityManager: EntityManager = this.entityManager) {
        return entityManager.getRepository(Checker)
    }
}
