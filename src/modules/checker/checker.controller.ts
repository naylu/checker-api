import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common'
import { Private } from 'src/common/auth'
import { EmployeeService } from '../employee/employee.service'
import { CheckerService } from './checker.service'
import { CreateCheckerInput } from './create-checker.input'
import { ListCheckerInput } from './list-checker'

@Controller('checker')
export class CheckerController {
    constructor(
        private checkerService: CheckerService,
        private employeeService: EmployeeService,
    ) {}

    @UseGuards(Private)
    @Get()
    list() {
        return this.checkerService.list()
    }

    @UseGuards(Private)
    @Post('list')
    async find(@Body() listCheckerInput: ListCheckerInput) {
        const { date, employeeId } = listCheckerInput
        let employeeData = await this.employeeService.getEmployee(employeeId)
        if (!employeeData) {
            throw new Error('Employee incorrect')
        }
        return this.checkerService.getCheckerDate(date, employeeData)
    }

    @Post()
    async create(@Body() createCheckerInput: CreateCheckerInput) {
        const { employeeId, checkCode, ...checkerData } = createCheckerInput
        let employeeData = await this.employeeService.getEmployee(employeeId)
        if (!employeeData) {
            throw new Error('Employee incorrect')
        }
        if (checkCode != employeeData.checkCode) {
            throw new Error('Checkcode incorrect')
        }
        return this.checkerService.create({
            employee: employeeData,
            ...checkerData,
        })
    }
}
