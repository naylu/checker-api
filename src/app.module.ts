import { Module } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { TypeOrmModule } from '@nestjs/typeorm'
import { AuthModule } from './common/auth'
import { CheckerModule } from './modules/checker/checker.module'
import { EmployeeModule } from './modules/employee/employee.module'
import { RoleScheduleModule } from './modules/role-schedule/role-schedule.module'
import { buildConfig } from './ormconfig'

@Module({
    imports: [
        ConfigModule.forRoot(),
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: (configService: ConfigService) =>
                buildConfig({
                    host: configService.get('DB_HOST'),
                    port: +configService.get<number>('DB_PORT'),
                    username: configService.get('DB_USER'),
                    password: configService.get('DB_PASS'),
                    database: configService.get('DB_NAME'),
                }),
            inject: [ConfigService],
        }),
        AuthModule,
        RoleScheduleModule,
        EmployeeModule,
        CheckerModule,
    ],
    controllers: [],
    providers: [],
})
export class AppModule {}
